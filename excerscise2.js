// No.1
const myFunction = (a,b) => {
  return a + b
}


//  No.2
const myFunction = (a,b) => {
  return a === b
}


// No.3

 const myFunction = (a) => {
  return typeof a
 }


//  No.4
function myFunction(a, n) {
    return a[n - 1];
 }
 const myFunction(a, n) {
  return a[n - 1]
 }

//  No.5
function myFunction(a) {
    return a.slice(3);
 }

//  No.6
function myFunction(str) {
    return str.slice(-3);
 }

// No.7
function myFunction(a) {
    return a.slice(0, 3);
 }

//  No.8
function myFunction(a) {
    return a.indexOf('is');
 }

//  No.9
function myFunction(a) {
    return a.slice(0, a.length / 2);
 }

//  No.10
function myFunction(a) {
    return a.slice(0, -3);
 }

//  No.11
function myFunction(a, b) {
    return b / 100 * a
  }

//   No.12
function myFunction(a, b) {
    return a.indexOf(b) === -1 ? a + b : b + a
  }

//   No.13
function myFunction(a, b, c, d, e, f) {
    return (((a + b - c) * d) / e) ** f;
 }

//  nO. 14
function myFunction(a) {
    return a % 2 === 0
  }

//   nO. 15
function myFunction(a, b) {
    return b.split(a).length - 1
}

// No. 16
function myFunction(a) {
    return a - Math.floor(a) === 0
  }

//   No. 17
function myFunction(a, b) {
    return a < b ? a / b : a * b
  }

//   No.18
function myFunction(a) {
    return Number(a.toFixed(2));
  }

//  No.19
function myFunction( a ) {
    const string = a + '';
    const strings = string.split('');
    return strings.map(digit => Number(digit))
  }

//   No.20


// ARRAY

// No. 1
function myFunction(a, n) {
    return a[n - 1];
}

// No.2 
function myFunction(a) {
    return a.slice(3);
 }

//  No.3
function myFunction(a) {
    return a.slice(-3);
 }

//  No.4
function myFunction(a) {
    return a.slice(0, 3);
 }

//  No.5
function myFunction(a, n) {
    return a.slice(-n);
  }

// No.6
function myFunction( a, b ) {
    return a.filter(cur => cur !== b)
  }

//   No.7
function myFunction(a) {
    return a.length;
 }

//  No.8
function myFunction(a) {
    return a.filter((ne) => ne < 0).length;
 }

//  No. 28
function myFunction( arr ) {
    return arr.sort()
    }

// No. 29
function myFunction( arr ) {
    return arr.sort((a, b) => b - a)
  }

//   No.30
function myFunction(a) {
    return a.reduce((acc, cur) => acc + cur, 0);
 }

//  No. 31
function myFunction( arr ) {
    return arr.reduce((acc, cur) => acc + cur, 0) / arr.length
    }

//Javascript objects
    
// function myFunction(a) {
//     return typeof a;
//  }

//  console.log(myFunction(null))

// const myFunction = (a) => typeof(a)

// console.log(myFunction("Yahuu"))



// const myFunction = (a) => a.slice(-3);

// console.log (myFunction("namasaya"))

// const myFunction = (a) => a.indexOf("is")

// console.log(myFunction("praise"))


// const myFunction = (arr) => {
//     return arr.reduce((a,b) => a.length < b.length ? b : a)
// }
// console.log(myFunction(['help', 'me']))


// const myFunction = (arr) => {
//     return 
// }

// const myFunction = (arr) => {
//  return new Set(arr).size === 1
// }

// console.log(myFunction([1,1,1,1]))

// No. 34

// const myFunction = (...arrays) => {
//     let array1 = arrays[0]
//     let array2 = arrays[1]

//     let arrayConcat = array1.concat(array2)
//     return arrayConcat
// }
// console.log(myFunction([1, 2, 3], [4, 5, 6]))

// const myFunction1 = (...arrays1) => {
//     return arrays1.flat()
// }
// console.log(myFunction1([1, 2, 3], [4, 5, 6]))

// // No. 35

// const myFunction = (arr) => {
//     const sortedArrays = (x,y) => x.b - y.b;
//     return arr.sort(sortedArrays)
// }

// console.log(myFunction([{a:1,b:2},{a:5,b:4}]))

// // No.36

// const myFunction = (a,b) => {
//     let merged = a.concat(b)
//     let noDuplicate = Array.from(new Set(merged))
//     let sorted = noDuplicate.sort ((a,b) => a - b ); 

//     return sorted
// }

// console.log(myFunction([-10, 22, 333, 42], [-11, 5, 22, 41, 42]))


// -----OBJECT-----
// // nO.37

// const myFunction = (obj) => {
//     return obj.country
// }

// console.log(myFunction({ continent: 'Asia', country: 'Japan' })
// )

// // No.38
// const myFunction = (obj) => {
//     return obj['prop-2']
// }

// console.log (myFunction({  'prop-2': 'two',  pro

// // No. 39
// const myFunction = (obj, key) => {
//     return obj[key]
// } 

// console.log(myFunction({  continent: 'Asia',  country: 'Japan'}, 'continent'))


// // No. 40
// const myFunction = (a,b) => {
//     return a.hasOwnProperty(b)
// }
// console.log(myFunction({a:1,b:2,c:3},'b'))

// No. 41
// const myFunction = (a,b) => {
//     return Object.values(a).includes(b)
// }

// console.log(myFunction({x:'a',y:null,z:'c'},'y'))

// // No.41
// const myFunction = (a) => {
//     return {key: a}
// }

// console.log(myFunction('z'))

// // No.42 Write a function that takes two strings (a and b) as arguments. Create an object that has a property with key 'a' and a value of 'b'. Return the object.

// const myFunction = (a,b) => {
//     return { [a]: b };
// }
// console.log(myFunction('a','b'))


// No.43
// const myFunction = (a,b) => {
//     const object = {}
// for (let i= 0; i < a.length; i++) {
// object[a[i]] = b[i]}
// return object
// }

// console.log(myFunction(['a','b','c'],[1,2,3]))

// cara 2
// const myFunction = (a,b) => {

// return a.reduce((acc, cur, i) => ({ ...acc, [cur]: b[i] }), {});
// }

// console.log(myFunction(['a','b','c'],[1,2,3]))

    


 